const Student = require('../models/StudentModel')
const getAllStudents = async (req, res) => {
    const data = await Student.getAll();
    res.json(data);
};
const getStudentById = async (req, res) =>{
    const id = req.params.id;
    const data = await Student.getById(id);
    res.json(data);
};
const createStudent = async (req, res) =>{

    const {id, name} = req.body;
    let student = new Student(id, name);
    const code = await student.create();
    res.sendStatus(code);
};

const changeStudent =  async (req, res) =>{
    
    const id = req.params.id;
    const name = req.body.name;
    let student = new Student(id, name);
    const code =  await student.save();
    res.sendStatus(code);
};

const deleteStudent = async (req, res)=>{
    const id = req.params.id;
    let student = new Student(id);
    const code = await student.delete();
    res.sendStatus(code);
};


module.exports = {
    getAllStudents,
    getStudentById,
    createStudent,
    changeStudent,
    deleteStudent
}