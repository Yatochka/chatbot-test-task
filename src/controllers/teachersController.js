const Teacher = require('../models/TeacherModel')
const getAllTeachers = async (req, res) => {
    const data = await Teacher.getAll();
    res.json(data);
};
const getTeacherById = async (req, res) =>{
    const id = req.params.id;
    const data = await Teacher.getById(id);
    res.json(data);
};
const createTeacher = async (req, res) =>{

    const {id, name, topic} = req.body;
    let teacher = new Teacher(id, name, topic);
    const code = await teacher.create();
    res.sendStatus(code);
};

const changeTeacher =  async (req, res) =>{
    
    const id = req.params.id;
    const {name, topic} = req.body;
    let teacher = new Teacher(id,name,topic);
    const code =  await teacher.save();
    res.sendStatus(code);
};

const deleteTeacher = async (req, res)=>{
    const id = req.params.id;
    let teacher = new Teacher(id);
    const code = await teacher.delete();
    res.sendStatus(code);
};


module.exports = {
    getAllTeachers,
    getTeacherById,
    createTeacher,
    changeTeacher,
    deleteTeacher
}