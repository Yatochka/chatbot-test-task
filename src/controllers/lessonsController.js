const Lesson = require('../models/LessonModel');
const getAllLessons = async (req, res) => {
    const data = await Lesson.getAll();
    res.json(data);
};
const getLessonById = async (req, res) =>{
    const id = req.params.id;
    const data = await Lesson.getById(id);
    res.json(data);
};
const createLesson = async (req, res) =>{

    const {id, topic, teacher, aud, grp} = req.body;
    let lesson = new Lesson(id, topic,teacher,aud,grp);
    const code = await lesson.create();
    res.sendStatus(code);
};

const changeLesson =  async (req, res) =>{
    
    const id = req.params.id;
    const {topic, teacher, aud, grp} = req.body;
    let lesson = new Lesson(id, topic,teacher,aud,grp);
    const code =  await lesson.save();
    res.sendStatus(code);
};

const deleteLesson = async (req, res)=>{
    const id = req.params.id;
    let lesson = new Lesson(id);
    const code = await lesson.delete();
    res.sendStatus(code);
};


module.exports = {
    getAllLessons,
    getLessonById,
    createLesson,
    changeLesson,
    deleteLesson
}