const Group = require('../models/GroupModel')
const getAllGroups = async (req, res) => {
    const data = await Group.getAll();
    res.json(data);
};
const getGroupById = async (req, res) =>{
    const id = req.params.id;
    const data = await Group.getById(id);
    res.json(data);
};
const createGroup = async (req, res) =>{

    const {id, name, student} = req.body;
    let group = new Group(id, name,student);
    const code = await group.create();
    res.sendStatus(code);
};

const changeGroup =  async (req, res) =>{
    
    const id = req.params.id;
    const {name,student} = req.body;
    let group = new Group(id, name, student);
    const code =  await group.save();
    res.sendStatus(code);
};

const deleteGroup = async (req, res)=>{
    const id = req.params.id;
    let group = new Group(id);
    const code = await group.delete();
    res.sendStatus(code);
};


module.exports = {
    getAllGroups,
    getGroupById,
    createGroup,
    changeGroup,
    deleteGroup
}