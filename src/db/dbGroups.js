const pool = require('./dbConnection');
const status = require('../helpers/status')

const getAll = () => {
    return new Promise((resolve, reject)=>{
        pool.query('Select  uni_groups.group_name, students.fullname From uni_groups inner join students on uni_groups.student = students.Id_Card')
        .then((res)=>{
            resolve(res.rows);
        })
        .catch((err)=>{
            reject(err);
        })
    });
    
};

const getById = (id) =>{
    return new Promise((resolve, reject)=>{
        pool.query ("Select  uni_groups.group_name, students.fullname From uni_groups inner join students on uni_groups.student = students.Id_Card Where group_name = $1",[id])
        .then((res)=>{
            resolve(res.rows);
        })
        .catch((err)=>{
            reject(err);
        })
    })
};

const create = (id,name, student) =>{
    return new Promise((resolve, reject)=>{
        pool.query(" Insert Into uni_groups (g_id, group_name, student) Values ($1, $2,$3)", [id, name, student])
        .then(()=>{
            resolve(status.created);
        })
        .catch((err)=>{
            reject(err);
        })      
    })
};

const change = (params) =>{
    return new Promise((resolve, reject)=>{
        pool.query("Update uni_groups Set group_name = $1, student = $2 Where g_id = $3",params)
        .then(()=>{
            resolve(status.success);
        })
        .catch((err)=>{
            reject(err);
        })
    })
};

const deleteGroup = (id)=>{
    return new Promise((resolve, reject)=>{
        pool.query("Delete From uni_groups Where group_name = $1",[id])
        .then(()=>{
            resolve(status.success);
        })
        .catch((err)=>{
            reject(err);
        })
    })
}


module.exports = {
    getAll,
    getById,
    create,
    change,
    deleteGroup
};