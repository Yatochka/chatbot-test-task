const pool = require('./dbConnection');
const status = require('../helpers/status')

const getAll = () => {
    return new Promise((resolve, reject)=>{
        pool.query('Select  * From teachers')
        .then((res)=>{
            resolve(res.rows);
        })
        .catch((err)=>{
            reject(err);
        })
    });
    
};

const getById = (id) =>{
    return new Promise((resolve, reject)=>{
        pool.query ("Select * From teachers Where t_id = $1",[id])
        .then((res)=>{
            resolve(res.rows);
        })
        .catch((err)=>{
            reject(err);
        })
    })
};

const create = (id, name, topic) =>{
    return new Promise((resolve, reject)=>{
        pool.query(" Insert Into teachers (t_id, teacher_name, specialization) Values ($1, $2,$3)", [id, name, topic])
        .then(()=>{
            resolve(status.created);
        })
        .catch((err)=>{
            reject(err);
        })      
    })
};

const change = (params) =>{
    return new Promise((resolve, reject)=>{
        pool.query("Update teachers Set teacher_name = $1, specialization = $2 Where t_id = $3", params)
        .then(()=>{
            resolve(status.success);
        })
        .catch((err)=>{
            reject(err);
        })
    })
};

const deleteTeacher = (id)=>{
    return new Promise((resolve, reject)=>{
        pool.query("Delete From teachers Where t_id = $1",[id])
        .then(()=>{
            resolve(status.success);
        })
        .catch((err)=>{
            reject(err);
        })
    })
}


module.exports = {
    getAll,
    getById,
    create,
    change,
    deleteTeacher
}