const Pool = require('pg').Pool
const config = require('../config')

const pool = new Pool({
    user: config.pg.user,
    host: config.pg.host,
    database: config.pg.dbname,
    password: config.pg.password,
    port: config.pg.port
});
/*
шоб не повторяти один і той самий шаблон в кожному запиту
const query = (command, params)=>{
    return new Promise((resolve, reject)=>{
        pool.query(command, params)
        .then((res)=>{
            resolve(res.rows);
        })
        .catch((err)=>{
            reject(err);
        })
    });
};*/

module.exports = pool;