const pool = require('./dbConnection');
const status = require('../helpers/status')

const getAll = () => {
    return new Promise((resolve, reject)=>{
        pool.query('Select  * From students')
        .then((res)=>{
            resolve(res.rows);
        })
        .catch((err)=>{
            reject(err);
        })
    });
    
};

const getById = (id) =>{
    return new Promise((resolve, reject)=>{
        pool.query ("Select * From students Where Id_Card = $1",[id])
        .then((res)=>{
            resolve(res.rows);
        })
        .catch((err)=>{
            reject(err);
        })
    })
};

const create = (id, name) =>{
    return new Promise((resolve, reject)=>{
        pool.query(" Insert Into students (Id_Card, fullname) Values ($1, $2)", [id, name])
        .then(()=>{
            resolve(status.created);
        })
        .catch((err)=>{
            reject(err);
        })      
    })
};

const change = (id, name) =>{
    return new Promise((resolve, reject)=>{
        pool.query("Update students Set fullname = $1 Where Id_Card = $2",[name, id])
        .then(()=>{
            resolve(status.success);
        })
        .catch((err)=>{
            reject(err);
        })
    })
};

const deleteStudent = (id)=>{
    return new Promise((resolve, reject)=>{
        pool.query("Delete From students Where Id_Card = $1",[id])
        .then(()=>{
            resolve(status.success);
        })
        .catch((err)=>{
            reject(err);
        })
    })
}


module.exports = {
    getAll,
    getById,
    create,
    change,
    deleteStudent
}