const pool = require('./dbConnection');
const status = require('../helpers/status')

const getAll = () => {
    return new Promise((resolve, reject)=>{
        pool.query('Select * From lessons')
        .then((res)=>{
            resolve(res.rows);
        })
        .catch((err)=>{
            reject(err);
        })
    });
    
};

const getById = (id) =>{
    return new Promise((resolve, reject)=>{
        pool.query ("Select * From lessons Where lesson_number = $1",[id])
        .then((res)=>{
            resolve(res.rows);
        })
        .catch((err)=>{
            reject(err);
        })
    })
};

const create = (id, topic, teacher, aud, grp) =>{
    return new Promise((resolve, reject)=>{
        pool.query(" Insert Into lessons (lesson_number, topic, teacher, aud, uni_group) Values ($1, $2,$3,$4,$5)", [id, topic, teacher, aud, grp])
        .then(()=>{
            resolve(status.created);
        })
        .catch((err)=>{
            reject(err);
        })      
    })
};

const change = (params) =>{
    return new Promise((resolve, reject)=>{
        pool.query("Update lessons Set topic = $1, teacher = $2, aud = $3, uni_group = $4 Where lesson_number = $5",params)
        .then(()=>{
            resolve(status.success);
        })
        .catch((err)=>{
            reject(err);
        })
    })
};

const deleteGroup = (id)=>{
    return new Promise((resolve, reject)=>{
        pool.query("Delete From lessons Where lesson_number = $1",[id])
        .then(()=>{
            resolve(status.success);
        })
        .catch((err)=>{
            reject(err);
        })
    })
}


module.exports = {
    getAll,
    getById,
    create,
    change,
    deleteGroup
};