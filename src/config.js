let config = module.exports
require('dotenv').config();

config.express = {
    port: process.env.PORT || 3000,
    ip: process.env.IP || '127.0.0.1'
};

config.pg = {
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    host: process.env.DB_HOST,
    port:process.env. DB_PORT,
    dbname: process.env.DB_NAME
}

