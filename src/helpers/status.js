const status = {
    success : 200,
    error: 500,
    notFound: 404, 
    created: 201,
    bad: 400
};

module.exports =  status;