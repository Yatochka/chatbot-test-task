const cnt = require('../controllers/studentsController');
const express = require('express')
const app = express()

const studentsRouter = express.Router()

studentsRouter.get("/", cnt.getAllStudents);
studentsRouter.get("/:id",cnt.getStudentById);
studentsRouter.post("/",cnt.createStudent);
studentsRouter.put("/:id",cnt.changeStudent);
studentsRouter.delete("/:id",cnt.deleteStudent);

module.exports = studentsRouter;