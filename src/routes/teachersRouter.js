const cnt = require('../controllers/teachersController');
const express = require('express')
const app = express()

const teachersRouter = express.Router()

teachersRouter.get("/", cnt.getAllTeachers);
teachersRouter.get("/:id",cnt.getTeacherById);
teachersRouter.post("/",cnt.createTeacher);
teachersRouter.put("/:id",cnt.changeTeacher);
teachersRouter.delete("/:id",cnt.deleteTeacher);

module.exports = teachersRouter;