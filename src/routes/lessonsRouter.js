const cnt = require('../controllers/lessonsController');
const express = require('express')
const app = express()

const lessonsRouter = express.Router()

lessonsRouter.get("/", cnt.getAllLessons);
lessonsRouter.get("/:id",cnt.getLessonById);
lessonsRouter.post("/",cnt.createLesson);
lessonsRouter.put("/:id",cnt.changeLesson);
lessonsRouter.delete("/:id",cnt.deleteLesson);

module.exports = lessonsRouter;