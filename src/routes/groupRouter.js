const cnt = require('../controllers/groupsController');
const express = require('express')
const app = express()

const groupsRouter = express.Router()

groupsRouter.get("/", cnt.getAllGroups);
groupsRouter.get("/:id",cnt.getGroupById);
groupsRouter.post("/",cnt.createGroup);
groupsRouter.put("/:id",cnt.changeGroup);
groupsRouter.delete("/:id",cnt.deleteGroup);

module.exports = groupsRouter;