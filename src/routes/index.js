const express = require("express");
const bodyParser = require('body-parser')
const students = require('./studentsRouter');
const teachers = require('./teachersRouter');
const groups = require('./groupRouter');
const lessons = require('./lessonsRouter');
const app = express();
const apiRouter = express.Router()
app.use(bodyParser.json())
app.use(
  bodyParser.urlencoded({
    extended: true,
  }))


apiRouter.use('/students',students);
apiRouter.use('/teachers',teachers);
apiRouter.use('/groups',groups);
apiRouter.use('/lessons',lessons)

app.use('/api',apiRouter);
apiRouter.use('/',(req,res)=>{
  res.sendFile(__dirname +'\\index.html')
})
app.get("/", function (request, response) {
  response.send('<a href = "http://localhost:3000/api">API\'s </a>');
});
module.exports = app;