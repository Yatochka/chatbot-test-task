const db = require('../db/dbLessons');

module.exports = class Lesson{
    constructor(id, topic, teacher, aud, grp){
        this.id = id;
        this.topic = topic;
        this.teacher = teacher;
        this.aud = aud;
        this.grp = grp;
    }
    create(){
        return  db.create(this.id,this.topic, this.teacher, this.aud, this.grp);
    }
    save(){
        return db.change([this.topic,this.teacher, this.aud, this.grp, this.id])
    }
    delete(){
        return db.deleteGroup(this.id);
    }
    static getAll(){
        return db.getAll();
    }
    static getById(id){
        return db.getById(id);
    }
}