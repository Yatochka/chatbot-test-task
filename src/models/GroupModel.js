const db = require('../db/dbGroups');

module.exports = class Group{
    constructor(id, name, student){
        this.id = id;
        this.name = name;
        this.student = student;
    }
    create(){
        return db.create(this.id, this.name, this.student);
    }
    save(){
        return db.change([this.name, this.student, this.id])
    }
    delete(){
        return db.deleteGroup(this.id);
    }
    static getAll(){
        return db.getAll();
    }
    static getById(id){
        return db.getById(id);
    }
}