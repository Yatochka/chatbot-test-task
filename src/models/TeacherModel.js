const db = require('../db/dbTeachers');

module.exports = class Teacher{
    constructor(id, name, topic){
        this.id = id;
        this.name = name;
        this.topic = topic;
    }
    create(){
        return db.create(this.id,this.name,this.topic);
    }
    save(){
        return db.change([this.name,this.topic,this.id])
    }
    delete(){
        return db.deleteTeacher(this.id);
    }
    static getAll(){
        return db.getAll();
    }
    static getById(id){
        return db.getById(id);
    }
}