const db = require('../db/dbStudents');

module.exports = class Student{
    constructor(id, name){
        this.id = id;
        this.name = name;
    }
    create(){
        return db.create(this.id, this.name);
    }
    save(){
        return db.change(this.id, this.name);
    }
    delete(){
        return db.deleteStudent(this.id);
    }
    static getAll(){
        return db.getAll()
    }
    static getById(id){
        return db.getById(id)
    }
};